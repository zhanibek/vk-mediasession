# Changelog

## v0.0.8 (2022-08-22)

- VK Audio: add default artwork if audio doesn't have one
- VK Audio: use playlist name as album name
- VK Audio: fix error when audio does not have artwork
- VK Audio: disable conflicting (and buggy) vk's own mediaSession implementation. this commit is ugly monkeypatch =)
- Yandex.Music: add default artwork if audio doesn't have one
- Yandex.Music: fix error when audio does not have artwork
- Yandex.Music: add more artwork sizes
- Yandex.Music: change playback state on event, not in handler
- Yandex.Music: content script matches any music.yandex subdomain

## v0.0.7 (2021-02-17)

- Fixed VK Audio
- Make extension less verbose

## v0.0.5 (2019-04-22)

- Added support for Yandex.Music
- Changed extension name and icon to more generic, because now it is not only VK specific.

## v0.0.4 (2019-02-06)

- Fixed: When tab restores from discard, sometimes script is not executed.

## v0.0.3 (2019-01-26)

- Resolved issue with prev, next controls.

## v0.0.2 (2019-01-26)

- Stability and compatibility improvements.

## v0.0.1 (2019-11-29)

- First release.
